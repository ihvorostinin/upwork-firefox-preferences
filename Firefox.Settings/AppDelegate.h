//
//  AppDelegate.h
//  Firefox.Settings
//
//  Created by Ivan Kh on 09/02/2018.
//  Copyright © 2018 ikhvorostinin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

