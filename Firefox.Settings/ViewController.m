//
//  ViewController.m
//  Firefox.Settings
//
//  Created by Ivan Kh on 09/02/2018.
//  Copyright © 2018 ikhvorostinin. All rights reserved.
//

#import "ViewController.h"
#import "Firefox.h"

@interface ViewController ()

@property Firefox * firefox;

@property (weak) IBOutlet NSTextField * tabUrlTextField;
@property (weak) IBOutlet NSTextField * homepageUrlTextField;
@property (weak) IBOutlet NSTextField * searchEngineTextField;

@end

@implementation ViewController

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)viewDidLoad {
    NSError * error;
    
    [super viewDidLoad];
    
    self.firefox = [Firefox firefoxWithError:&error];
    [self.firefox read:&error];
    
    self.homepageUrlTextField.stringValue = self.firefox.homepageUrlString.length
    ? self.firefox.homepageUrlString
    : @"";
    
    self.searchEngineTextField.stringValue = self.firefox.searchCurrentEngine ? self.firefox.searchCurrentEngine : @"";
    
    if (error) {
        [self presentError:error];
    }
    
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Actions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (IBAction)applyButtonAction:(id)sender {
    NSError * error;

    self.firefox.homepageUrlString = self.homepageUrlTextField.stringValue;
    self.firefox.searchCurrentEngine = self.searchEngineTextField.stringValue;
    [self.firefox write:&error];
    
    if (error) {
        [self presentError:error];
    }
}

@end
