//
//  Utils.h
//  Firefox.Settings
//
//  Created by Ivan Kh on 12/02/2018.
//  Copyright © 2018 ikhvorostinin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Crypto)

- (NSData *)sha256;

@end

