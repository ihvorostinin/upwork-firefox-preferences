//
//  Utils.m
//  Firefox.Settings
//
//  Created by Ivan Kh on 12/02/2018.
//  Copyright © 2018 ikhvorostinin. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import "Utils.h"

@implementation NSData (Crypto)

- (NSData *)sha256 {
    NSMutableData *macOut = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(self.bytes, (unsigned int)self.length, macOut.mutableBytes);
    return macOut;
}

@end

