//
//  Firefox.m
//  Firefox.Settings
//
//  Created by Ivan Kh on 09/02/2018.
//  Copyright © 2018 ikhvorostinin. All rights reserved.
//

#import "lz4.h"
#import "Utils.h"
#import "Firefox.h"

static NSString * const tFirefoxProfilesPath = @"~/Library/Application Support/Firefox/Profiles";
static NSString * const tFirefoxPrefsFileName = @"prefs.js";
static NSString * const tFirefoxSearchFileName = @"search.json.mozlz4";

static NSString * const tFirefoxDefaultProfileFolderExtension = @"default";
static NSString * const tUanableToFindHomepageUrlSetting = @"Unable to find homepage URL setting.";
static NSString * const tUanableToFindPreferenceFile = @"Unable to find Firefox preferences path.";
static NSString * const tHomepageUrlRegex = @"user_pref\\(\"browser\\.startup\\.homepage\", \"(.*)\"\\);";
static NSString * const tHomepageUrlSetting = @"user_pref(\"browser.startup.homepage\", \"%@\");";

static NSString * const tMozillaLZ4Header = @"mozLz40\0";
static NSString * const tVerificationHashSalt = @"\
%@%@\
By modifying this file, I agree that I am doing so only within Firefox itself, \
using official, user-driven search engine selection processes, and in a way which does not circumvent user consent. \
I acknowledge that any attempt to change this file from outside of Firefox is a malicious act, \
and will be responded to accordingly.";

@interface NSData (Firefox)

- (NSData *)mlz4CompressedData;
- (NSData *)mlz4DecompressedData;

@end

@implementation NSData (Firefox)

- (NSData *)mlz4CompressedData {
    uint32_t length = (uint32_t)self.length;
    NSData * lengthData = [NSData dataWithBytes:&length length:4];
    NSData * headerData = [tMozillaLZ4Header dataUsingEncoding:NSUTF8StringEncoding];
    uint32_t encodedLength = LZ4_compressBound(length);
    char   * encodedBytes = (char *)malloc(encodedLength);
    
    memset(encodedBytes, 0, encodedLength);
    
    uint32_t actualEncodedLength = LZ4_compress_default((char *)self.bytes, encodedBytes, length, encodedLength);
    
    if (actualEncodedLength == 0) {
        return nil;
    }
    
    NSData * encodedData = [NSData dataWithBytesNoCopy:encodedBytes length:actualEncodedLength freeWhenDone:YES];
    NSMutableData * result = [NSMutableData new];
    
    [result appendData:headerData];
    [result appendData:lengthData];
    [result appendData:encodedData];
    
    return result;
}

- (NSData *)mlz4DecompressedData {
    NSData * headerData = [self subdataWithRange:NSMakeRange(0, 8)];
    NSString * headerString = [NSString.alloc initWithData:headerData encoding:NSUTF8StringEncoding];
    
    if ([headerString isEqualToString:tMozillaLZ4Header] == NO) {
        return nil;
    }
    
    char *encodedDataBytes = (char *)self.bytes;
    
    size_t decodedDataSize = *(uint32_t *) (encodedDataBytes + 8);
    char * decodedDataBytes = (char *)malloc(decodedDataSize);
    
    memset(decodedDataBytes, 0, decodedDataSize);
    
    if (!decodedDataBytes) {
        return nil;
    }
    
    if (LZ4_decompress_safe_partial(encodedDataBytes + 12,
                                    decodedDataBytes,
                                    (int)self.length - 12,
                                    (int)decodedDataSize,
                                    (int)decodedDataSize) < 0) {
        return nil;
    }
    
    return [NSData dataWithBytesNoCopy:decodedDataBytes length:decodedDataSize freeWhenDone:YES];
}

@end

@interface Firefox ()

@property NSURL * defaultProfileURL;
@property NSURL * prefsURL;
@property NSURL * searchURL;
@property NSRegularExpression * homepageUrlRegex;

@property NSMutableDictionary * searchPreferences;
@property (nonatomic) NSArray<NSString *> * searchEngines;

@end

@implementation Firefox

- (id)init {
    NSError * error;
    return [self initWithError:&error];
}

- (id)initWithError:(NSError **)error {
    self = [super init];
    
    if (*error == nil) {
        [self setupDefaultProfile:error];
    }
    
    if (*error == nil) {
        [self setupPrefsJS:error];
        [self setupSearch:error];
    }
    
    return self;
}

+ (instancetype)firefoxWithError:(NSError **)error {
    return [self.alloc initWithError:error];
}

- (void)read:(NSError **)error {
    [self readPreferencesFromPrefsJS:error];
    [self readPreferencesFromSearch:error];
}

- (void)write:(NSError **)error {
    [self writePreferencesToPrefsJS:error];
    [self writePreferencesToSearch:error];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Utils
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)getVerificationHash:(NSString *)aName {
    return [[[[NSString stringWithFormat:tVerificationHashSalt, self.defaultProfileURL.lastPathComponent, aName]
              dataUsingEncoding:NSUTF8StringEncoding]
             sha256]
            base64EncodedStringWithOptions:0];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Profile
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setupDefaultProfile:(NSError **)error {
    NSString * defaultProfilePath;
    NSString * profilesPath = tFirefoxProfilesPath.stringByExpandingTildeInPath;
    
    // actually we should use profiles.ini file to find default profile path
    for (NSString * fileName in [NSFileManager.defaultManager contentsOfDirectoryAtPath:profilesPath
                                                                                  error:error]) {
        if ([fileName.pathExtension isEqualToString:tFirefoxDefaultProfileFolderExtension]) {
            defaultProfilePath = [profilesPath stringByAppendingPathComponent:fileName];
            break;
        }
    }
    
    if (*error) {
        return;
    }
    
    if (defaultProfilePath == nil) {
        *error = [NSError errorWithDomain:@""
                                     code:0
                                 userInfo:@{ NSLocalizedDescriptionKey : tUanableToFindPreferenceFile }];
        return;
    }
    
    self.defaultProfileURL = [NSURL fileURLWithPath:defaultProfilePath];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - prefs.js
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setupPrefsJS:(NSError **)error {

    // prefs.js
    
    NSURL * url = [self.defaultProfileURL URLByAppendingPathComponent:tFirefoxPrefsFileName];
    
    if ([NSFileManager.defaultManager fileExistsAtPath:url.path] == NO) {
        *error = [NSError errorWithDomain:@""
                                     code:0
                                 userInfo:@{ NSLocalizedDescriptionKey : tUanableToFindPreferenceFile }];
        return;
    }
    
    self.prefsURL = url;
    
    // regex
    
    self.homepageUrlRegex = [NSRegularExpression regularExpressionWithPattern:tHomepageUrlRegex
                                                                      options:NSRegularExpressionCaseInsensitive
                                                                        error:error];
}

- (NSString *)readPrefsJS:(NSError **)error {
    return [NSString stringWithContentsOfURL:self.prefsURL
                                    encoding:NSUTF8StringEncoding
                                       error:error];
}

- (void)writePrefsJS:(NSString *)content error:(NSError **)error {
    [content writeToURL:self.prefsURL
             atomically:YES
               encoding:NSUTF8StringEncoding
                  error:error];
}

- (NSRange)rangeOfHomepageURLInText:(NSString *)text error:(NSError **)error {
    NSTextCheckingResult * firstMatch = [self.homepageUrlRegex firstMatchInString:text
                                                                          options:0
                                                                            range:NSMakeRange(0, text.length)];
    
    if (firstMatch.numberOfRanges != 2) {
        *error = [NSError errorWithDomain:@""
                                     code:0
                                 userInfo:@{ NSLocalizedDescriptionKey : tUanableToFindHomepageUrlSetting }];
        return NSMakeRange(0, 0);
    }
    
    return [firstMatch rangeAtIndex:1];
}

- (void)readPreferencesFromPrefsJS:(NSError **)error {
    NSError * internalError;
    NSString * prefsJS = [self readPrefsJS:error];
    NSRange range = [self rangeOfHomepageURLInText:prefsJS error:&internalError];
    
    if (range.location == 0) {
        return;
    }
    
    self.homepageUrlString = [prefsJS substringWithRange:range];
}

- (void)writePreferencesToPrefsJS:(NSError **)error {
    NSError * internalError;
    NSString * prefsJS = [self readPrefsJS:error];
    NSString * newValue = self.homepageUrlString;
    NSRange oldValueRange = [self rangeOfHomepageURLInText:prefsJS error:&internalError];
    
    if (internalError || oldValueRange.location == 0) {
        NSString * newSetting = [NSString stringWithFormat:tHomepageUrlSetting,
                                 self.homepageUrlString ? self.homepageUrlString : @""];
        prefsJS = [prefsJS stringByAppendingFormat:@"\n%@", newSetting];
    }
    else {
        prefsJS = [prefsJS stringByReplacingCharactersInRange:oldValueRange withString:newValue];
    }
    
    [self writePrefsJS:prefsJS error:error];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - search.json.mozlz4
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setupSearch:(NSError **)error {
    
    // path
    
    NSURL * url = [self.defaultProfileURL URLByAppendingPathComponent:tFirefoxSearchFileName];
    
    if ([NSFileManager.defaultManager fileExistsAtPath:url.path] == NO) {
        *error = [NSError errorWithDomain:@""
                                     code:0
                                 userInfo:@{ NSLocalizedDescriptionKey : tUanableToFindPreferenceFile }];
        return;
    }
    
    self.searchURL = url;
}

- (id)readSearch:(NSError **)error {
    NSData * encodedData = [NSData dataWithContentsOfURL:self.searchURL];
    NSData * decodedData = [encodedData mlz4DecompressedData];
    
//    [decodedData writeToFile:@"/Users/ivankh/Downloads/search_inp.json" atomically:YES];

    return [NSJSONSerialization JSONObjectWithData:decodedData
                                           options:NSJSONReadingMutableContainers
                                             error:error];
}

- (void)writeSearch:(id)content error:(NSError **)error {
    NSData * initialData = [NSJSONSerialization dataWithJSONObject:content options:0 error:error];
    NSData * encodedData = [initialData mlz4CompressedData];
    
//    [initialData writeToFile:@"/Users/ivankh/Downloads/search_out.json" atomically:YES];
    
    [encodedData writeToFile:self.searchURL.path atomically:YES];
}

- (void)readPreferencesFromSearch:(NSError **)error {
    NSMutableDictionary * obj = [self readSearch:error];
    
    self.searchEngines = obj[@"engines"];
    self.searchCurrentEngine = obj[@"metaData"][@"current"];
    
    if (self.searchCurrentEngine.length == 0) {
        self.searchCurrentEngine = obj[@"metaData"][@"searchDefault"];
    }
}

- (void)writePreferencesToSearch:(NSError **)error {
    NSMutableDictionary * obj = [self readSearch:error];
    NSMutableArray * engines = obj[@"engines"];

    NSMutableDictionary * currentEngine;
    NSURL * currentEngineURL = [NSURL URLWithString:self.searchCurrentEngine];
    NSString * currentEngineName = currentEngineURL.host;
    
    if (currentEngineURL == nil) {
        return;
    }
    
    for (NSMutableDictionary * engine in engines) {
        if ([engine[@"_name"] isEqualToString:currentEngineName]) {
            currentEngine = engine;
            break;
        }
    }
    
    if (currentEngine == nil) {
        currentEngine = [NSMutableDictionary new];
        currentEngine[@"_name"] = currentEngineName;
        currentEngine[@"_shortName"] = currentEngineName;
        currentEngine[@"_iconURL"] = @"";
        currentEngine[@"_readOnly"] = @(NO);
        currentEngine[@"_metaData"] = @{};

        [engines addObject:currentEngine];
    }

    NSMutableDictionary * url = [NSMutableDictionary new];
    url[@"template"] = [self.searchCurrentEngine stringByAppendingString:@"{searchTerms}"];
    url[@"resultDomain"] = currentEngineURL.host;
    currentEngine[@"_urls"] = @[ url ];
    
    obj[@"metaData"][@"current"] = currentEngineName;
    obj[@"metaData"][@"hash"] = [self getVerificationHash:currentEngineName];
    
    [self writeSearch:obj error:error];
}

@end
