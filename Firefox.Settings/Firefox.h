//
//  Firefox.h
//  Firefox.Settings
//
//  Created by Ivan Kh on 09/02/2018.
//  Copyright © 2018 ikhvorostinin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Firefox : NSObject

@property (nonatomic) NSString * homepageUrlString;
@property (nonatomic, readonly) NSArray<NSString *> * searchEngines;
@property (nonatomic) NSString * searchCurrentEngine;

+ (instancetype)firefoxWithError:(NSError **)error;

- (void)read:(NSError **)error;
- (void)write:(NSError **)error;

@end
